import Card from "../Card/Card";

function ProductList({ toggleFavorite, products, favorite, handleModal }) {

   return (
      <>
         {
            products.map(elem => {
               return (
                  <Card  toggleFavorite={toggleFavorite} elem={elem} key={elem.article} isFavorite={favorite.includes(elem.article)} handleModal={handleModal} />
               )
            })
         }
      </>
   )
}
export default ProductList;