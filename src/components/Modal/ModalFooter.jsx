import Button from "../Button/Button";
import styles from './Modal.module.css'

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
     return (
          <div className="modal-footer">
               {
                  firstText && <Button className={styles.modalCloseButton} onClick={firstClick}>{firstText}</Button>
               }
               {
                  secondaryText && <Button className={styles.modalCloseButton} onClick={secondaryClick}>{secondaryText}</Button>
               }
          </div>
     )
}
export default ModalFooter;