import PropTypes from 'prop-types';

function Header({ favorite, cart }) {

     return (
          <header>
               <h3>Favorite {favorite.length}</h3>
               <h3>Cart {cart.length}</h3>
          </header>
     )
}
export default Header;
Header.propTypes = {
     favorite: PropTypes.array.isRequired,
     cart: PropTypes.array.isRequired,
};