import Button from '../Button/Button';
import styles from './Card.module.css';
import { useState, useEffect } from 'react';

function Card({ toggleFavorite, elem, isFavorite, handleModal }) {
     const buttonText = isFavorite ? <i className="fas fa-star"></i> : <i className="far fa-star"></i>;

     return (
          <li className={styles.card}>
               <h3>{elem.name}</h3>
               <img src={elem.src} alt="pic" />
               <span>{elem.price}</span>
               <h4>Товар є в таких кольорах:</h4>
               <p>{elem.color}</p>
               <Button className={'first-btn'} onClick={toggleFavorite.bind(null, elem.article)}>
                    {buttonText}
               </Button>
               <Button className={'second-btn'} onClick={() => handleModal(elem.article)}>
                    add to busket
               </Button>
          </li>
     )
}
export default Card;