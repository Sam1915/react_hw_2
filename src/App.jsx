import React, { useLayoutEffect } from 'react';
import { useState, useEffect } from 'react';
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';
import ModalFooter from './components/Modal/ModalFooter';
import ProductList from './components/ProductList/ProductList';
import Header from './components/Header/Header';
import productStyles from './Index.module.css';
import productListStyles from './components/ProductList/ProductList';


function App() {
   const localCart = localStorage.getItem('cart')
   const initialCart = localCart ? JSON.parse(localCart) : [];
   const [openModal, setOpenModal] = useState(null); // 
   const [products, setProducts] = useState([]);
   const [favorite, setFavorite] = useState([]);
   const [cart, setCart] = useState(initialCart);

   function toggleFavorite(article) {
      setFavorite((prev) => {
         const isFavorite = prev.includes(article);
         if (isFavorite) {
            return prev.filter((productArticle) => productArticle !== article);
         } else {
            // const newArr = [];
            // for (let i = 0; i <= prev.length - 1; i++) {
            //    newArr.push(prev[i]);
            // }
            // newArr.push(article);
            // return newArr;
            return [...prev, article];
         }
      });
   }

   const handleModal = (article) => {
      setOpenModal(article);
   }

   useEffect(() => {
      const favLocalArr = localStorage.getItem('favorite');
      if (favLocalArr) {
         setFavorite(JSON.parse(favLocalArr));
      }
      fetch('./products.json')
         .then(response => response.json())
         .then(data => setProducts(data));
   }, []);

   useEffect(() => {
      localStorage.setItem('favorite', JSON.stringify(favorite));
      localStorage.setItem('cart', JSON.stringify(cart));
   }, [favorite, cart]);


   function addToCart() {
      setCart((prev) => {
         return [...prev, openModal];
      })
   }

   return (
      <div className={productStyles.products}>
         <Header favorite={favorite} cart={cart} />
         <ProductList className={productListStyles.productList} toggleFavorite={toggleFavorite} products={products} favorite={favorite} handleModal={handleModal} />
         {openModal && <ModalImage addToCart={addToCart} onClose={() => handleModal(null)}></ModalImage>}
      </div>
   );
};

export default App;